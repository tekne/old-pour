/*!
Utility enums
*/

/// A unary result, which can be either a new value or the old value
///
/// Used to avoid cloning
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum UnaryResult<T> {
    /// A new value
    New(T),
    /// The old value
    Old,
}

/// A binary result, which can be either the left value, the right value, or a new value
///
/// Used to avoid cloning
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum BinaryResult<T> {
    /// A new value
    New(T),
    /// The left value
    Left,
    /// The right value
    Right,
    /// Either the left or the right, don't care
    Ambi,
}

impl<T> BinaryResult<T> {
    /// Convert an `Option` to a `New` or a `Left`
    pub fn or_left(opt: Option<T>) -> BinaryResult<T> {
        opt.map(BinaryResult::New).unwrap_or(BinaryResult::Left)
    }
    /// Convert an `Option` to a `New` or a `Right`
    pub fn or_right(opt: Option<T>) -> BinaryResult<T> {
        opt.map(BinaryResult::New).unwrap_or(BinaryResult::Right)
    }
    /// Unwrap a `BinaryResult<T>`, panicking if not `New`
    pub fn unwrap(self) -> T {
        match self {
            BinaryResult::New(n) => n,
            _ => panic!("Expected result to be New(_)!"),
        }
    }
    /// Unwrap a `BinaryResult<T>`, cloning if not `New`
    pub fn unwrap_or_clone(self, left: &T, right: &T) -> T
    where
        T: Clone,
    {
        match self {
            BinaryResult::New(n) => n,
            BinaryResult::Left => left.clone(),
            BinaryResult::Right => right.clone(),
            BinaryResult::Ambi => left.clone(),
        }
    }
    /// Map a `BinaryResult` (as a functor)
    pub fn map<F, U>(self, func: F) -> BinaryResult<U>
    where
        F: FnOnce(T) -> U,
    {
        match self {
            BinaryResult::New(t) => BinaryResult::New(func(t)),
            BinaryResult::Left => BinaryResult::Left,
            BinaryResult::Right => BinaryResult::Right,
            BinaryResult::Ambi => BinaryResult::Ambi,
        }
    }
    /// Swap the sides of a `BinaryResult`, i.e. flip `Left` and `Right`
    pub fn swap_sides(self) -> BinaryResult<T> {
        match self {
            BinaryResult::New(t) => BinaryResult::New(t),
            BinaryResult::Left => BinaryResult::Right,
            BinaryResult::Right => BinaryResult::Left,
            BinaryResult::Ambi => BinaryResult::Ambi,
        }
    }
}
