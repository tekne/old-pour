/*!
A simple cons context implemented using a `hashbrown` `HashMap` for efficient access to the raw entry API, which is
very helpful for hash consing.
*/
use super::*;
use ahash::RandomState;
use hashbrown::HashMap;
use ref_cast::RefCast;
use std::hash::BuildHasher;

/// A (deep) consing context implemented using a `hashbrown` `HashMap`
#[derive(Debug, Clone, Eq, PartialEq, RefCast)]
#[repr(transparent)]
pub struct MapCtx<K: RadixKey + Eq + Hash, V: Clone + Eq + Hash, S: BuildHasher = RandomState>(
    pub HashMap<Arc<InnerMap<K, V>>, (), S>,
);

impl<K: RadixKey + Eq + Hash, V: Clone + Eq + Hash, S: BuildHasher> ConsCtx<K, V>
    for MapCtx<K, V, S>
{
    fn cons(&mut self, inner: InnerMap<K, V>) -> Arc<InnerMap<K, V>> {
        let (key, _value) = self
            .0
            .raw_entry_mut()
            .from_key(&inner)
            .or_insert_with(|| (Arc::new(inner), ()));
        key.clone()
    }
    fn cons_arc(&mut self, inner: &Arc<InnerMap<K, V>>) -> Arc<InnerMap<K, V>> {
        let (key, _value) = self
            .0
            .raw_entry_mut()
            .from_key(inner)
            .or_insert_with(|| (inner.clone(), ()));
        key.clone()
    }
}

impl<K: RadixKey + Eq + Hash, V: Clone + Eq + Hash, S: BuildHasher + Default> Default
    for MapCtx<K, V, S>
{
    fn default() -> MapCtx<K, V, S> {
        MapCtx(HashMap::default())
    }
}

impl<K: RadixKey + Eq + Hash, V: Clone + Eq + Hash> MapCtx<K, V> {
    /// Create a new, empty `MapCtx`
    pub fn new() -> MapCtx<K, V> {
        MapCtx::default()
    }
}
