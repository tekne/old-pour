/*!
Basic `IdMap` performance tests
*/
use ahash::RandomState;
use criterion::{black_box, criterion_group, criterion_main, BenchmarkId, Criterion, Throughput};
use pour::IdMap;
use rand::{Rng, SeedableRng};
use rand_xoshiro::Xoroshiro128PlusPlus;
use std::collections::HashSet;

fn bench_insert_u64(c: &mut Criterion) {
    let mut rng = Xoroshiro128PlusPlus::from_entropy();
    let mut group = c.benchmark_group("insert_random_u64_null");
    for log_size in 0..10 {
        let elements = 1 << log_size;
        group.throughput(Throughput::Elements(elements));
        group.bench_with_input(
            BenchmarkId::from_parameter(elements),
            &elements,
            |b, &elements| {
                b.iter(|| {
                    let mut x = IdMap::<u64, ()>::new();
                    for _ in 0..elements {
                        assert!(x.insert(rng.gen(), ()));
                    }
                    black_box(x)
                });
            },
        );
    }
    group.finish();
    let mut group = c.benchmark_group("insert_random_u64_u64");
    for log_size in 0..10 {
        let elements = 1 << log_size;
        group.throughput(Throughput::Elements(elements));
        group.bench_with_input(
            BenchmarkId::from_parameter(elements),
            &elements,
            |b, &elements| {
                b.iter(|| {
                    let mut x = IdMap::<u64, u64>::new();
                    for i in 0..elements {
                        assert!(x.insert(rng.gen(), i));
                    }
                    black_box(x)
                });
            },
        );
    }
    group.finish();
    let mut group = c.benchmark_group("insert_ordered_u64_u64");
    for log_size in 0..10 {
        let elements = 1 << log_size;
        group.throughput(Throughput::Elements(elements));
        group.bench_with_input(
            BenchmarkId::from_parameter(elements),
            &elements,
            |b, &elements| {
                b.iter(|| {
                    let mut x = IdMap::<u64, u64>::new();
                    for i in 0..elements {
                        x.insert(i, i);
                    }
                    black_box(x)
                });
            },
        );
    }
    group.finish();
}

fn bench_set_op_u64(c: &mut Criterion) {
    let mut rng = Xoroshiro128PlusPlus::from_entropy();
    let mut group = c.benchmark_group("random_hashmap_set_ops_u64_u64");
    group.measurement_time(std::time::Duration::new(10, 0));
    for log_size in 5..10 {
        let elements = 1 << log_size;
        group.bench_with_input(
            BenchmarkId::from_parameter(elements),
            &elements,
            |b, &elements| {
                let mut x = HashSet::<u64, RandomState>::default();
                x.reserve(elements);
                for _ in 0..elements {
                    x.insert(rng.gen());
                }
                let mut y = HashSet::<u64, RandomState>::default();
                x.reserve(elements);
                for _ in 0..elements {
                    y.insert(rng.gen());
                }
                b.iter(|| black_box(black_box(&x).union(black_box(&y)).copied().collect::<HashSet<u64>>()));
            },
        );
    }
    group.finish();
    let mut group = c.benchmark_group("random_set_ops_u64_u64");
    group.measurement_time(std::time::Duration::new(10, 0));
    for log_size in 5..10 {
        let elements = 1 << log_size;
        group.bench_with_input(
            BenchmarkId::from_parameter(elements),
            &elements,
            |b, &elements| {
                let mut x = IdMap::<u64, ()>::new();
                for _ in 0..elements {
                    x.insert(rng.gen(), ());
                }
                let mut y = IdMap::<u64, ()>::new();
                for _ in 0..elements {
                    y.insert(rng.gen(), ());
                }
                b.iter(|| black_box(black_box(&x).unioned(black_box(&y))));
            },
        );
    }
    group.finish();
}

criterion_group!(benches, bench_set_op_u64, bench_insert_u64);
criterion_main!(benches);
