# 0.2.1 (November 13, 2020)

- Mistake: forgot to rename `update_in`

# 0.2.0 (November 13, 2020)

- Changed confusing `insert` API, renaming it to `try_insert`. Renamed `update` to `insert_conservative` and added proper `insert` functions.

# 0.1.3 (November 13, 2020)

- Added support for more kinds of `RadixKey`
- Made domain subset checks work on maps with heterogeneous value types
- Added checks for disjoint map domains (`domains_intersect` and `domains_disjoint`)

# 0.1.2 (November 3, 2020)

- Added `clear` operation to `IdMap` for GC purposes.
- Added tests for `InnerMap` ordering

# 0.1.1 (November 2, 2020)

- Made `InnerMap` implement `Ord` and `PartialOrd` (by component addresses) for use with `flurry`

# 0.1.0

- Initial release